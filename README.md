# psyn Environment
version 0.1.0
|
[VisionMise](mailto:visionmise@psyn.app)
|
Deno Module
|
TypeScript

## psyn Application Environment Support Library
A set of static functions for working with a local project environment

---

## Install

There is no reason to install psyn Environment. It's use case is only as a support library.


## Usage

#### Import from [psyn.app](https://psyn.app)

This is the bundled version:

```typescript
    import { psynEnv } from "https://psyn.app/environment.js"
```

OR

#### Import from [gitlab.com](https://gitlab.com/psyn/environment/-/blob/master/src/psyn_environment.ts)

This is the development version:
```typescript
    import { psynEnv} from "https://gitlab.com/psyn/environment/-/blob/master/src/psyn_environment.ts"
```

---

## Static Methods

## Append File

Appends an existing file with the given content. File must exist to be appended.

```typescript
    static async appendFile (path: string): Promise<boolean>
```

## Announce Error

Sends a message to the console error log using `console.error`

```typescript
    static async announceError (errorMessage: string): void
```

## Announce Status

Sends a message to the console log using `console.log`

```typescript
    static announceStatus(statusMessage: string): void
```

## Create Folder

Creates a new folder for the given path. Optionally recursive

```typescript
    static async createFolder(folder: string, recursive: boolean = false): Promise<boolean>
```

## Path Exists

Checks if the given path (file or folder) exists

```typescript
    static async pathExists(path: string): Promise<boolean>
```

## Read File

Reads all content from the given filepath if the file exists

```typescript
    static async readFile(filepath: string): Promise<string>
```

## Run Command

Executes a command using `Deno.run` and returns the output from the process upon process termination

```typescript
    static async runCommand(command: string): Promise<string>
```

## Write File

Writes the given content to the given file. Overwrites existing files. Creates new files when they do not exist.

```typescript
    static async writeFile(filepath: string, content: string):  Promise<number>
```
