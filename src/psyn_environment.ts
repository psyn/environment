/** 
 * psyn Environment
 * Static Functions for Working with a local project environment
 * @author VisionMise <visionmise@psyn.app>
 * @version 0.1.0
 */

/** psynEnv Helper Class */
export class psynEnv {

  /** pathExists Checks if given the file or folder exists */
  static async pathExists(path: string): Promise<boolean> {

    try {

      //try to get the path stat
      const meta = await Deno.stat(path);

      //if there is meta, the path must exist
      if (meta) return true;

    } catch (errorMessage) {

      //something happened, may not have permission --allow-read
      return false;
    }

    //no meta found for given path
    return false;
  }

  /** createFolder Creates a folder with given path */
  static async createFolder(folder: string, recursive: boolean = false): Promise<boolean> {

    //if the folder exists
    if (await psynEnv.pathExists(folder)) {

      //let the user know
      psynEnv.announceError(`Cannot create folder '${folder}'. Folder already exists`);
      return false;
    }

    //create new folder options
    const folderOptions: Deno.MkdirOptions = { "recursive": recursive };

    //try to create a folder
    try {

      //ask deno to make a folder. wait for it
      await Deno.mkdir(folder, folderOptions);

    } catch (errorMessage) {

      //user cannot create folders. let them know it
      psynEnv.announceError(errorMessage);
      return false;

    }

    //return true if the folder now exists
    return await psynEnv.pathExists(folder);
  }

  /** readFile Gets the content of the given path */
  static async readFile(filepath: string): Promise<string> {
    try {

      //get file content
      const output: Uint8Array = await Deno.readFile(filepath);

      //decode the typed output
      const content: string = new TextDecoder().decode(output);

      //return file content
      return content;

    } catch (errorMessage) {

      //announce read error
      psynEnv.announceError(`Could not read file: ${filepath}`);
      return "";
    }
  }

  /** writeFile creates a file with the given content. Overwrites existing files */
  static async writeFile(filepath: string, content: string): Promise<number> {

    //set file options
    const fileOptions: Deno.OpenOptions = {
      "createNew": true,
      "truncate": true,
      "write": true,
    };

    try {

      //ask deno to open filepath
      const file: Deno.File = await Deno.open(filepath, fileOptions);

      //encode content
      const input: Uint8Array = new TextEncoder().encode(content);

      //write content to file
      const size: number = await file.write(input);

      //close file
      file.close();

      //return filesize
      return size;
    } catch (errorMessage) {

      //announce error
      psynEnv.announceError(`Could not write file: ${filepath}`);
      return -1;
    }
  }

  /** appendFile appends existing file with given content */
  static async appendFile(filepath: string, content: string): Promise<number> {

    //set file options
    const fileOptions: Deno.OpenOptions = {
      "createNew": false,
      "truncate": false,
      "append": true,
    };

    try {

      //ask Deno to open the file. wait for it
      const file: Deno.File = await Deno.open(filepath, fileOptions);

      //encode content to typed array
      const input: Uint8Array = new TextEncoder().encode(content);

      //append file with content
      const size: number = await file.write(input);

      //close file
      file.close();

      //return size
      return size;

    } catch (errorMessage) {

      //announce error
      psynEnv.announceError(`Could not append file: ${filepath}`);
      return -1;
    }
  }

  /** runCommand executes the given command as a Deno Process and returns output */
  static async runCommand(command: string): Promise<string> {

    //set runtime options
    const runtimeOptions: Deno.RunOptions = {
      "cmd": command.split(" "),
      "stdout": "piped",
    };

    try {

      //ask Deno to run the given command. wait for it
      const proc: Deno.Process = await Deno.run(runtimeOptions);

      //get the process output
      const response: Uint8Array = await proc.output();

      //decode process output
      const output: string = new TextDecoder().decode(response);

      //return output string
      return output;

    } catch (errorMessage) {

      //announce error
      psynEnv.announceError(`Could not execute command: ${command}`);
      return "";
    }
  }

  /** announceError prints an error message to the console */
  static announceError(errorMessage: string) {
    console.error(`❗️ ${errorMessage}`);
  }

  /** announceStatus prints a message to the console */
  static announceStatus(statusMessage: string) {
    console.log(`${statusMessage}`);
  }
}
