#!/bin/sh
clear
echo "Building psyn Environment Module"
[ -d build/psyn ] && echo "Directory Exists!" || mkdir -p build/psyn
deno bundle src/psyn_environment.ts > build/psyn/environment.js
echo "Done"